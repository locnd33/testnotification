import React from 'react';
import { Notifications, Permissions, Constants } from 'expo';
import { Text, View, Button, NativeModules, Share } from 'react-native';
import { Toast } from "native-base";
export default class App extends React.Component {
  static navigationOptions = { header: null }
  state = {
    notification: {},
    token: '',
  };

  registerForPushNotificationsAsync = async () => {
    if (Constants.isDevice) {
      const { status: existingStatus } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
      );
      let finalStatus = existingStatus;
      if (existingStatus !== 'granted') {
        const { status } = await Permissions.askAsync(
          Permissions.NOTIFICATIONS
        );
        finalStatus = status;
      }
      if (finalStatus !== 'granted') {
        alert('Failed to get push token for push notification!');
        return;
      }
      let token = await Notifications.getExpoPushTokenAsync();
      console.log(token);
      this.setState({ token });
    } else {
      alert('Must use physical device for Push Notifications');
    }
  };

  componentDidMount() {
    this.registerForPushNotificationsAsync();
    this._notificationSubscription = Notifications.addListener(
      this._handleNotification
    );
  }

  _handleNotification = notification => {
    this.setState({ notification: notification });
  };

  sendPushNotification = async () => {
    const message = {
      to: this.state.token,
      sound: 'default',
      title: 'Notification Title for me !',
      body: 'Notification Body for me !',
      ios: { sound: true },
      priority: 'high',
      data: { data: 'goes here' },
    };
    const response = await fetch('https://exp.host/--/api/v2/push/send', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Accept-encoding': 'gzip, deflate',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(message),
    });
    const data = response._bodyInit;
    console.log(data, "data")
    if (this.state.notification && this.state.notification.origin) {
      alert(this.state.notification.origin + " notification ");
    }
  };
  onShare = async () => {
    try {
      const result = await Share.share({
        message: this.state.token
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };
  render() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'space-around',
        }}>
        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
          <Text>Origin: {this.state.notification.origin}</Text>
          <Text>Data: {JSON.stringify(this.state.notification.data)}</Text>
          <Text>Token:</Text>
          <Text>{this.state.token}</Text>
        </View>
        <Button
          title={'Lưu token'}
          onPress={() => this.onShare()}
        />
        <Button
          title={'Press to Send Notification'}
          onPress={() => this.sendPushNotification()}
        />
      </View>
    );
  }
}